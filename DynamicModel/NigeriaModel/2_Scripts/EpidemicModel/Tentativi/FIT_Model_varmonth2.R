source("DynamiqueEquation_varmonth2.R")



###########################

DIFF_name <- "Dynamic Model"

DIFF_theta.names <- c("alpha","mu","epsilon0","epsilon1","epsilon2","epsilon3","incoming","exploit","nu","eta","rho","vacc","imm","beta","p")
epistates<-c("S","E","I","R","D","V","Z")
nagegroupx<-5
DIFF_state.names <- c("Imm")
for(st in epistates){
  for(i in 0:(nagegroupx-1) ){
    state<-paste(st,i,sep="")
    DIFF_state.names<-c(DIFF_state.names,state)
  }
}
######################################################
InfectionDynamique <- function(theta,init.state,times) {



  # resulat transformés en dataframe
  traj <- as.data.frame(ode(init.state, times, Infection_dde, theta, method = "iteration"))


  # newD est le cumulative des morts. On transform en le nombre de mort par jour
  #traj <- mutate(traj,NewD=c(0,diff(NewD)))
  last<-nrow(traj)
  
  #Sero0<-(traj[last,]$R0)/(traj$S0+traj$E0+traj$I0+traj$R0)
  Sero1<-traj[last,]$R1/(traj[last,]$S1+traj[last,]$E1+traj[last,]$I1+traj[last,]$R1)
  Sero2<-traj[last,]$R2/(traj[last,]$S2+traj[last,]$E2+traj[last,]$I2+traj[last,]$R2)
  Sero3<-traj[last,]$R3/(traj[last,]$S3+traj[last,]$E3+traj[last,]$I3+traj[last,]$R3)
  Sero4<-traj[last,]$R4/(traj[last,]$S4+traj[last,]$E4+traj[last,]$I4+traj[last,]$R4)
  
  
  prob<-c(Sero1,Sero2,Sero3,Sero4)

  # print(c(traj[last,]$R0,traj[last,]$Imm,traj[last,]$R1,traj[last,]$R2,traj[last,]$R3,traj[last,]$R4,traj[last,]$R5))
  Age<-c("0-12","12-18","18-24",">24")
  traj2<-data.frame("Age"=Age,"prob"=prob)
 
  newlist<-list("traj2"=traj2,"simulation"=traj)
  
  return(newlist)
  
}

##function to compute log-prior
DIFF_prior <- function(theta,parameters_to_estimate,limits, log = FALSE) {
 
  log.sum <-0
  for(p in parameters_to_estimate){
    #print(p)
   # print(as.numeric(theta[[p]]))
    #print(limits[["lower"]][[p]])
    log.sum <-log.sum+ dunif(as.numeric(theta[[p]]), min = limits[["lower"]][[p]], max = limits[["upper"]][[p]], log = TRUE)
    
  }
  
  return(log.sum)
}

DIFF_pointLike <- function(data.point, model.point, theta, log = FALSE){
  ## the prevalence is observed through a binomial process
  xvec<-as.numeric(data.point[["Positive"]])
  svec<-as.numeric(data.point[["Population"]])
  
  pvec<-as.numeric(model.point[["prob"]])
#
  logpoint<-dbinom(x = xvec,size= svec,prob=pvec,log = TRUE)
  #print(c(pvec,svec,xvec,logpoint))
  return(logpoint)
}
DIFF_genObsPoint <- function(model.point, theta,data){
   
  ## the prevalence is observed through a binomial process
  
  obs.point <- rbinom(n = 1,size= data[["Population"]], prob = model.point[["prob"]])
  
  return(c(obs = obs.point))
}

DIFF <- fitmodel(
  name = DIFF_name,
  state.names = DIFF_state.names,
  theta.names = DIFF_theta.names,
  simulate = InfectionDynamique,
  dprior = DIFF_prior,
  rPointObs = DIFF_genObsPoint,
  dPointObs = DIFF_pointLike)
 
#########################################################################################