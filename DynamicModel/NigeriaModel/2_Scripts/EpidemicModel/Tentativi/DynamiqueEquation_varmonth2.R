

Infection_dde <- function(time, state, theta) {
  
  epsilon0 <- theta[["epsilon0"]]
  epsilon1 <- theta[["epsilon1"]]
  epsilon2 <- theta[["epsilon2"]]
  epsilon3 <- theta[["epsilon3"]]
  alpha <- theta[["alpha"]]#fertility
  mu <- theta[["mu"]]#mortality
  mu4 <- theta[["mu4"]]
  incoming <- theta[["incoming"]]
  exploit <- theta[["exploit"]]
  tab_effect<-theta[["tab_effect"]]
  seas_effect<-theta[["seas_effect"]]
  conf_effect<-theta[["conf_effect"]]
  
  exploit_eff<-ifelse(time%%365>181 & time%%365<272,tab_effect*exploit,exploit )
  mu_eff<-ifelse(time%%365>181 ,seas_effect*mu,mu)
  incoming_eff<-ifelse(time%%365<92 ,conf_effect*incoming,incoming)
  
  
  nu<-theta[["nu"]]
  eta<-theta[["eta"]]
  rho<-theta[["rho"]]
  
  imm<-theta[["imm"]]
  beta<-theta[["beta"]]
  beta1<-theta[["beta1"]]
  p<-theta[["p"]]
  padu<-theta[["padu"]]
  vacc<-theta[["vacc"]]
  
  
  
  
  
  # Compartments
  Imm <- state[["Imm"]]

  S0 <- state[["S0"]]
  S1 <- state[["S1"]]
  S2 <- state[["S2"]]
  S3 <- state[["S3"]]
  S4 <- state[["S4"]]

  

  E0 <- state[["E0"]]
  E1 <- state[["E1"]]
  E2 <- state[["E2"]]
  E3 <- state[["E3"]]
  E4 <- state[["E4"]]

  

  I0 <- state[["I0"]]
  I1 <- state[["I1"]]
  I2 <- state[["I2"]]
  I3 <- state[["I3"]]
  I4 <- state[["I4"]]


  R0 <- state[["R0"]]
  R1 <- state[["R1"]]
  R2 <- state[["R2"]]
  R3 <- state[["R3"]]
  R4 <- state[["R4"]]


  D0 <- state[["D0"]]
  D1 <- state[["D1"]]
  D2 <- state[["D2"]]
  D3 <- state[["D3"]]
  D4 <- state[["D4"]]

  

  V0 <- state[["V0"]]
  V1 <- state[["V1"]]
  V2 <- state[["V2"]]
  V3 <- state[["V3"]]
  V4 <- state[["V4"]]

  

  Z0 <- state[["Z0"]]
  Z1 <- state[["Z1"]]
  Z2 <- state[["Z2"]]
  Z3 <- state[["Z3"]]
  Z4 <- state[["Z4"]]

  ############################
  # Les equations
  ##########################

  N0<-Imm+S0+E0+I0+R0
  N1<-S1+E1+I1+R1
  N2<-S2+E2+I2+R2
  N3<-S3+E3+I3+R3
  N4<-S4+E4+I4+R4

  N<-N0+N1+N2+N3+N4
  Nj<-N0+N1+N2
  Nad<-N3+N4
  I<-I0+I1+I2+I3+I4
  Ij<-I0+I1+I2
  Iad<-I3+I4
  anticorps<-(R2+R3+R4+V2+V3+V4)
  mothersusc<-(N2+N3+N4)-anticorps
  ############################################
 
  NewImm<-Imm -(epsilon0+mu+rho+exploit)*Imm+incoming*Imm+alpha*imm*anticorps-(vacc)*Imm
  NewS0 <-S0 -(1-exp(-(beta*Ij/Nj +beta1*Iad/Nad )))*S0-(mu+epsilon0+exploit)*S0+incoming*S0+rho*Imm+alpha*((1-imm)*anticorps+mothersusc)
  NewE0 <-E0 +(1-exp(-(beta*Ij/Nj +beta1*Iad/Nad )))*S0-nu*E0-(mu+epsilon0+exploit)*E0+incoming*E0
  NewZ0 <-Z0 +(1-exp(-(beta*Ij/Nj +beta1*Iad/Nad )))*S0
  NewI0 <-I0 +nu*E0-eta*I0-(mu+epsilon0+exploit)*I0+incoming*I0
  NewR0 <-R0 +(1-p)*eta*I0-(mu+epsilon0+exploit)*R0+incoming*R0
  NewD0 <-D0 +(p)*eta*I0
  NewV0 <-V0 -(epsilon0+mu+exploit)*V0+incoming*V0
  
  NewS1 <-S1 -(1-exp(-(beta*Ij/Nj +beta1*Iad/Nad )))*S1+epsilon0*(S0)-(mu+epsilon1+exploit_eff)*S1+incoming*S1-vacc*S1
  NewE1 <-E1 +(1-exp(-(beta*Ij/Nj +beta1*Iad/Nad )))*S1-nu*E1+epsilon0*E0-(mu+epsilon1+exploit_eff)*E1+incoming*E1-vacc*E1
  NewZ1 <-Z1 +(1-exp(-(beta*Ij/Nj +beta1*Iad/Nad )))*S1
  NewI1 <-I1 +nu*E1-eta*I1+epsilon0*I0-(mu+epsilon1+exploit_eff)*I1+incoming*I1-vacc*I1
  NewR1 <-R1 +(1-p)*eta*I1+epsilon0*R0-(mu+epsilon1+exploit_eff)*R1+incoming*R1-vacc*R1
  NewD1 <-D1 +(p)*eta*I1
  NewV1 <-V1+ epsilon0*V0-(epsilon1+mu+exploit_eff)*V1+incoming*V1+vacc*(S1+E1+R1+I1)
  
  NewS2 <-S2 -(1-exp(-(beta*Ij/Nj +beta1*Iad/Nad )))*S2+epsilon1*(S1)-(mu_eff+epsilon2+exploit_eff)*S2+incoming_eff*S2-vacc*S2
  NewE2 <-E2 +(1-exp(-(beta*Ij/Nj +beta1*Iad/Nad )))*S2-nu*E2+epsilon1*E1-(mu_eff+epsilon2+exploit_eff)*E2+incoming_eff*E2-vacc*E2
  NewZ2 <-Z2 +(1-exp(-(beta*Ij/Nj +beta1*Iad/Nad )))*S2
  NewI2 <-I2 +nu*E2-eta*I2+epsilon1*I1-(mu_eff+epsilon2+exploit_eff)*I2+incoming_eff*I2-vacc*I2
  NewR2 <-R2 +(1-padu)*eta*I2+epsilon1*R1-(mu_eff+epsilon2+exploit_eff)*R2+incoming_eff*R2-vacc*R2
  NewD2 <-D2 +(padu)*eta*I2
  NewV2 <-V2 +epsilon1*V1-(epsilon2+mu_eff+exploit_eff)*V2+incoming_eff*V2+vacc*(S2+E2+R2+I2)
  
  NewS3 <-S3 -(1-exp(-(beta1*Ij/Nj +beta1*Iad/Nad )))*S3+epsilon2*(S2)-(mu_eff+epsilon3+exploit_eff)*S3+incoming_eff*S3-vacc*S3
  NewE3 <-E3 +(1-exp(-(beta1*Ij/Nj +beta1*Iad/Nad )))*S3-nu*E3+epsilon2*E2-(mu_eff+epsilon3+exploit_eff)*E3+incoming_eff*E3-vacc*E3
  NewZ3 <-Z3 +(1-exp(-(beta1*Ij/Nj +beta1*Iad/Nad )))*S3
  NewI3 <-I3 +nu*E3-eta*I3+epsilon2*I2-(mu_eff+epsilon3+exploit_eff)*I3+incoming_eff*I3-vacc*I3
  NewR3 <-R3 +(1-padu)*eta*I3+epsilon2*R2-(mu_eff+epsilon3+exploit_eff)*R3+incoming_eff*R3-vacc*R3
  NewD3 <-D3 +(padu)*eta*I3
  NewV3 <-V3+ epsilon2*V2-(epsilon3+mu_eff+exploit_eff)*V3+incoming_eff*V3+vacc*(S3+E3+R3+I3)
  
  NewS4 <-S4 -(1-exp(-(beta1*Ij/Nj +beta1*Iad/Nad )))*S4+epsilon3*(S3)-(mu4+exploit_eff)*S4+incoming*S4-vacc*S4
  NewE4 <-E4 +(1-exp(-(beta1*Ij/Nj +beta1*Iad/Nad )))*S4-nu*E4+epsilon3*E3-(mu4+exploit_eff)*E4+incoming*E4-vacc*E4
  NewZ4 <-Z4 +(1-exp(-(beta1*Ij/Nj +beta1*Iad/Nad )))*S4
  NewI4 <-I4 +nu*E4-eta*I4+epsilon3*I3-(mu4+exploit_eff)*I4+incoming*I4-vacc*I4
  NewR4 <-R4 +(1-padu)*eta*I4+epsilon3*R3-(mu4+exploit_eff)*R4+incoming*R4-vacc*R4
  NewD4 <-D4 +(padu)*eta*I4
  NewV4 <-V4+epsilon3*V3-(mu4+exploit_eff)*V4+incoming*V4+vacc*(S4+E4+R4+I4)
  #print(c(NewImm,NewS0))
  return(list(c(NewImm,NewS0,NewE0,NewI0,NewR0,NewD0,NewV0,NewZ0,NewS1,NewE1,NewI1,NewR1,NewD1,NewV1,NewZ1,NewS2,NewE2,NewI2,NewR2,NewD2,NewV2,NewZ2,NewS3,NewE3,NewI3,NewR3,NewD3,NewV3,NewZ3,NewS4,NewE4,NewI4,NewR4,NewD4,NewV4,NewZ4)))
}