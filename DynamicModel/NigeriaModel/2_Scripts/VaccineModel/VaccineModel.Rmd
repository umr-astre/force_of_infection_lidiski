---
title: "Model_vaccine"
author: "Andrea Apolloni"
date: "02/09/2021"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Settin up

```{r setdir}
rm(list=ls())

pathdemo<-"../../1_Data/"

pathout<-"../../3_Outputs/"
pathsero<-"../../1_Data/"
```

```{r packages,echo=FALSE}
library(cowplot)
library(ggplot2)
library(fitR)
library(coda)
library(plyr)
library(dplyr)
library(tmvtnorm)
library(reshape2)
library(stringr)
library(deSolve)
library(matlib)
library(lattice)
library(truncnorm)
```


```{r }
leavingfunction <- function(x, scale) {
  switch(scale,
         week = x,
         day = x/7,
         month = 1-(1-x)**4,
         year=1-(1-x)**52)
}

entryfunction <- function(x, scale) {
  switch(scale,
         week = x,
         day = x/7,
         month = (1+x)**(4)-1,
         year=(1+x)**(52)-1)
}
epsilonfun <- function(x, scale) {
  switch(scale,
         week = 1/(x*4),
         day = 1/(x*30),
         month = 1/x,
         year=1-(1-1/x)**12)
}
timefun <- function(x, scale) {
  t0=switch(scale,
         week = (x*52),
         day = (x*365),
         month = x*12,
         year=x)
  return(seq(0,t0))
}

etafun <- function(x, scale) {
  switch(scale,
         week = 1-(1-x)**7,
         day = x,
         month = 1-(1-x)**30,
         year=1-(1-x)**365)
}
rhofun <- function(x, scale) {
  switch(scale,
         week = 1-(1-x)**7,
         day = x,
         month = 1-(1-x)**30,
         year=1-(1-x)**365)
}

nufun <- function(x, scale) {
  switch(scale,
         week = 1-(1-x)**7,
         day = x,
         month = 1-(1-x)**30,
         year=1-(1-x)**365)
}





```
# Introduction

 Following the discussion with  Pacem in June , we develop a model taking account of the willingness to vaccinate of the livestock owners.
 In this model the small ruminants population is divided in 2 groups depending on the  fact of their owners being sensitive or not to the use of vaccine.  Animals belonging to sensitize owners will be identified by an index S , the others by an index N. Indicating with $p^S$ the fraction of sensitized  owners, the populations of the two groups are:
\begin{aligned}
N^S &= p^S*N \\
N^N &= (1-p^S)*N 
\end{aligned}
where N is the total PR population

## The 2 groups
The main difference between the two groups is their knowledge of the vaccine benefit (being sensitized) and their trust on the vaccine/ vaccinatorr. The second can be different between the two group and we indicate with $f^S,f^N$ respectivley the fraction of owners who trust the vaccine enough to vaccinate. Under these conditions the quantity of vaccines in the 2 groups can be estimated as:

\begin{aligned}
Q^S &= p^S f^S Q \qquad \textrm{vacc_S}=\frac{p^S f^S Q}{N^S}=\frac{ f^S Q}{N}\\
Q^N &= (1-p^S) f^N Q \qquad \textrm{vacc_N}=\frac{(1-p^S) f^N Q}{N^N}=\frac{ f^N Q}{N}
\end{aligned}

## The model's equation

We are going to simulate the diffusion of PPR in the Sylvo-patoral area of Senegal. We consider the 2 groups (sensitized and not) and for each one  an age stratified population (0-3 month, 3-12,12-18,18-24,24+). For each group we consider at the same time the demographic and transmission dynamics. Because of this we consider 5 epidemiological compartiments: Susceptible (S), Exposed (E), Infectious (I), Recovered (R),Dead because of the disease (D). To these compartiments we add an Immune (Imm) for the termporary immune newborn (compartiment 0). Immune loose their immunity after 3 months and become susceptibles.
Compartment for each group are then described using a pedex (0 to 4) and index (S,N)
 To these compartiments we add  some bookkeeping compartiments: total number of cases (Z); Vaccinated (V) but also 2 compartiments taking account of Income (Inc) and consumption (Cons). These 2 bookkeeping are connected to the outflow of animals in the following way 

For each group the equations governing the evolution are:
\begin{aligned}
\textrm{Inc}^S &= (q) \textrm{export} (S^S+E^S+I^S+R^S+V^S) \qquad \textrm{Cons}^S = (1-q) \textrm{export} (S^S+E^S+I^S+R^S+V^S)\\
\textrm{Inc}^N &= (q) \textrm{export} (S^N+E^N+I^N+R^N+V^N) \qquad \textrm{Cons}^N = (1-q) \textrm{export} (S^N+E^N+I^N+R^N+V^N)
\end{aligned}

where q is the probability of selling or consumming animal. For the time being  we consider that is the same for the 2 sub-populations.
In the following we consider the set of equations for one of the 2 sub-population. The pedex $i,j$ identified the age group (0 to 4)

We considered that only animals older than 1 year can procreate and  can be importe/exported for commercial reason

\begin{aligned}

\frac{d\textrm{Imm}^S}{dt} &=  - (\epsilon_0 + \mu)\textrm{Imm}^S  -\rho \textrm{Imm}^S+ \{\alpha \sum_{j > 1 }((\textrm{imm})*(R^S_j+V^S_j)) \} \\


\frac{dV^S_i}{dt} &= \delta (t-t_{vac})\textrm{vacc}^S (N^S_i) - (\epsilon_i + \mu+\textrm{export})V^S_i +\textrm{import}*V^S_i \\



\frac{dS^S_i}{dt} &= -\delta (t-t_{vac})\textrm{vacc}^S (S^S_i)-\beta S^S_i \sum_{j}(I^S_j+I^N_j) - (\epsilon_i + \mu+\textrm{export})S^S_i +\textrm{import} S^S_i +\rho \textrm{Imm}^S + (\epsilon_{i-1})S^S_{i-1}+\{\alpha \sum_{j \neq 0 }(S^S_j+E^S_j+I^S_j+(1-\textrm{imm})*(R^S_j+V^S_j)) \} \\


\frac{dZ^S_i}{dt} &= \beta S^S_i \sum_{j}(I^S_j+I^N_j)   \\





\frac{dE^S_i}{dt} &= -\delta (t-t_{vac})\textrm{vacc}^S (E^S_i)+\beta S^S_i  \sum_{j}(I^S_j+I^N_j) -\nu E^S_{i} - (\epsilon_i + \mu+\textrm{export})E^S_i +\textrm{import}E^S_i  + (\epsilon_{i-1})E^S_{i-1} \\

\frac{dI^S_i}{dt} &= -\delta (t-t_{vac})\textrm{vacc}^S (I^S_i)+\nu E_{i}-\eta I^S_{i} - (\epsilon_i + \mu+\textrm{export})I^S_i +\textrm{import}*I^S_i  + (\epsilon_{i-1})I^S_{i-1} \\

\frac{dR^S_i}{dt} &= -\delta (t-t_{vac})\textrm{vacc}^S (R^S_i)+(1-p)\eta I^S_{i} - (\epsilon_i + \mu+\textrm{export})R^S_i +\textrm{import}*R^S_i  + (\epsilon_{i-1})R^S_{i-1} \\



\frac{dD^S_i}{dt} &= +(p) \eta I^S_{i}\\



\frac{dInc^S}{dt} &= (q) \textrm{export} (S_i^S+E-i^S+I_i^S+R_i^S+V_i^S) \\

\frac{dComm^S}{dt} &= (1-q) \textrm{export} (S_i^S+E-i^S+I_i^S+R_i^S+V_i^S)


\end{aligned}

We included the tem $\delta$ that indicates that a quantity of vaccine is introduced in a specific period of the year. We also supposed that newborn animals, less than 3 months  are not vaccinated because proceted by maternal antibodies




Les differents parametres sont remport?s dans le tableau en bas

<!-- \begin{table}[ht] -->
<!-- \centering -->
<!-- \begin{tabular}{r|r|r|r} -->

  Parametre | Description                         | Valeur                     | Source
------------|-------------------------------------|----------------------------|------------
  $\mu$     | Mortality                           | Daily (5.6 e-05)                     | Average value form Baoba  datasets, in the Scenario  we consider varaition along the year
 $\epsilon_i$ | Aging parameter      | $\{1/90;1/275;1/180;1/180\}$          | Depend on the age class partition
$\rho$     | Matenal antibody loss    | $1/90$                    | Reciprocal of immunity perio
$\alpha$    | fertility rate                    | Daily 2.7 e-4| Average value form Baoba  datasets, in the Scenario  we consider variation along the year
$\nu$      | latency rate                     |$1/4$                    | Reciprocal of  latency days 
$\eta$      | Infectious rate                 |$1/7$                    | Reciprocal of infectious days 
imm      | Immunity rate at birth               |0.92                    | literature
import         | inflow rate                 | Daily  (2.535451e-04 )     | Average value form Baobab  datasets, in the Scenario  we consider varaition along the year
export         | outflow rate                 | Daily (2.698917e-04)          | Average value form Baobab  datasets, in the Scenario  we consider varaition along the year
vacc           | Fraction fo vaccianted animal         | Depends on availability         | 
$\beta$      |transmission rate                | to estmater    (around 0.4)              | calibration 
$p$        |fatality rate | 0.33-0.37                 | work on Mauritania
$q$        |probability of being sold | free parameter                 | socio-economic ECO-PPR
$f^s,f^N$        |proportion of population confident | free parameter                | socio-economic ECO-PPR
$p^S$        |proportion of sensitized owners| free parameter                | socio-economic ECO-PPR
<!-- \end{tabular} -->
<!-- \end{table} -->

The $\beta$ parameter has been estimated through the calibration of the  baseline model to the serological data  in the Sylvo-pastoral area collected by Vacnada

![Serological data (black dots) and model projection](../../3_Outputs/seroplot_uni3.png)

The initial condition are estimated  at the equilibrium in the baseline scenario, without the distinction among groups. 


## Estimation of the baseline distribution serological

To this aim, we consider the same demographic parameters  used for the calibration model to the serolgocial value. The beta parameters are those estimated from  the calibration and stored in the file "../3_Outputs/results_beta_uni3.csv". Since we are considering the baseline situation , where no vaccination is put in place we can consider a single population type divided in 5 age groups. The equations are stored in the file "2_Scripts/DynamiqueEquation_1group.r"

```{r calling equation 1 group}
source("DynamiqueEquation_1group.R")
```

## Demographic data
Demographic data have been extracted fro the Baobab datasets, while the age distribution of the small ruminant population come from the results of the population dynmaic model at the equilibrium
```{r}

demofilePR<-paste(pathdemo,"demo_par.csv",sep="/")
demoparameterPR<-read.csv(demofilePR,sep=",",header=TRUE)
demoparameterPR<-demoparameterPR %>%
  transmute(par,type,
            Mean = rowMeans(select(., X1:X12)))
demoparameterPR<-demoparameterPR[demoparameterPR$type=='Sylvo',]
filedemoPR<-paste(pathdemo,"PopDistrSylvo.csv",sep="/")
distrPR<-read.csv2(filedemoPR,sep=",",stringsAsFactors = FALSE)
names(distrPR)<-c("AgeClass","Distr")
distrPR<-distrPR %>%
  pull(as.numeric(Distr), AgeClass)
```

## paramatres fixes
```{r}

nyears<-25
scale<-"day"
times<-timefun(nyears,scale)
demosw<-demoparameterPR
alpha<-entryfunction(demosw[demosw$par=="reprod",]$Mean, scale)
incoming<-entryfunction(demosw[demosw$par=="hint",]$Mean, scale)
mu<-leavingfunction(demosw[demosw$par=="hdea",]$Mean, scale)
exploit<-leavingfunction(demosw[demosw$par=="hoff",]$Mean, scale)
epsilon0<-epsilonfun(3, scale)
epsilon1<-epsilonfun(9, scale)
epsilon2<-epsilonfun(6, scale)
epsilon3<-epsilonfun(6, scale)



rho<-rhofun(1/90,scale)
nu<-nufun(1/4,scale)
eta<-etafun(1/4,scale)

vacc<-0.0
imm<-0.92
p<-0.37

```

```{r importing beta data}
nvalues<-20
betavec0<-read.csv("../../3_Outputs/results_beta_uni3.csv")
betavec<-sample(betavec0[,2],nvalues)
```


```{r Initial condition 1 group}
nagegroupx<-5
distrsw<-as.numeric(distrPR)
pop<-10000
taux_infec<-0.01
taux_lat<-0.01
taux_rec<-0.0
Svector<-as.integer(pop*(1-taux_infec-taux_lat-taux_rec)*distrsw)
Evector<-as.integer(pop*(taux_lat)*distrsw)
Ivector<-as.integer(pop*(taux_infec)*distrsw)
Rvector<-as.integer(pop*(taux_rec)*distrsw)

Dvector<-c(0,0,0,0,0)

Vvector<-c(0,0,0,0,0)
Zvector<-c(0,0,0,0,0)
Imm<-0

init.state<-c(Imm)
namesstate<-c("Imm")
for(i in 0:(nagegroupx-1) ){
  state<-paste("S",i,sep="")
  namesstate<-c(namesstate,state)
  init.state<-c(init.state,Svector[i+1])
    state<-paste("E",i,sep="")
  namesstate<-c(namesstate,state)
  init.state<-c(init.state,Evector[i+1])
  state<-paste("I",i,sep="")
  namesstate<-c(namesstate,state)
  init.state<-c(init.state,Ivector[i+1])
    state<-paste("R",i,sep="")
   namesstate<-c(namesstate,state)
   init.state<-c(init.state,Rvector[i+1])
     state<-paste("D",i,sep="")
   namesstate<-c(namesstate,state)
   init.state<-c(init.state,Dvector[i+1])
  state<-paste("V",i,sep="")
  namesstate<-c(namesstate,state)
  init.state<-c(init.state,Vvector[i+1])
  state<-paste("Z",i,sep="")
  namesstate<-c(namesstate,state)
  init.state<-c(init.state,Zvector[i+1])
}  
names(init.state)<-namesstate
init.state0<-init.state
```

Simulating the evolution for the first group

```{r Simulating baseline}
result_baseline<-data.frame()
for (b in betavec){
  beta<-b
  print(beta)
  theta<-c("alpha"=alpha,"mu"=mu,"epsilon0"=epsilon0,"epsilon1"=epsilon1,"epsilon2"=epsilon2,"epsilon3"=epsilon3,"incoming"=incoming,"exploit"=exploit,"nu"=nu,"eta"=eta,"rho"=rho,"vacc"=vacc,"imm"=imm,"beta"=beta,"p"=p)
  init.state<-init.state0
  res<-InfectionDynamique(theta,init.state,times) 
  result_baseline<-rbind(result_baseline,res)
}

```

```{r simulating vacciantion}
demoparameterPR<-read.csv(demofilePR,sep=",",header=TRUE)
demoparameterPR<-demoparameterPR[demoparameterPR$type=='Sylvo',]
row.names(demoparameterPR)<-demoparameterPR[,1]
demoparameterPR[,1]<-NULL

alphavec<-demoparameterPR[demoparameterPR$par=="reprod",1:12]
names(alphavec)<-paste("alpha",seq(1,12),sep="_")

mortalityvec<-demoparameterPR[demoparameterPR$par=="hdea",1:12]
names(mortalityvec)<-paste("mu",seq(1,12),sep="_")

exploitvec<-demoparameterPR[demoparameterPR$par=="hoff",1:12]
names(exploitvec)<-paste("exploit",seq(1,12),sep="_")

incomingvec<-demoparameterPR[demoparameterPR$par=="hint",1:12]
names(incomingvec)<-paste("incoming",seq(1,12),sep="_")

```

```{r change time vaccination}
choosetimevacc<-function(month){
  res<-switch(month,"January"=15,"February"=46,"March"=74,"April"=95,"May"=125,"June"=156,"July"=186,"August"=217,"September"=248,"October"=278,"November"=318,"December"=348)
  return(res)
}
```

```{r preparing initial condition }
source("DynamiqueEquation_2groups_simpl.R")
source("InitialConditions_Simpl.R")
nagegroups<-2
ntype_el<-2
types_el<-c("S","N")
distrpop<-rep(c(distrsw[1]+distrsw[2],distrsw[3]+distrsw[4]+distrsw[5]),ntype_el)
pop<-10000
epsilon<-epsilonfun(12, scale)
vacc_cost<-1
feed<- 100 
price<- 1000 
q<-0.1
taux_infec<-0.01 
Q<-5000
vacc_month<-"October" 
timevacc<-choosetimevacc(vacc_month)
typevacc<-1
result_typevacc_1<-data.frame()
for(s in 0:3){
  ps<-0.25*(1+s)
  N_S<-as.integer(pop*ps)
  N_N<-as.integer((1-ps)*pop)
  distrpop<-as.integer(distrpop*rep(c(N_S,N_N),ntype_el))
  for(fs in 0:3){
    f_S<-0.25*(1+fs)
      for(fn in 0:3){
        f_N<-0.25*(1+fn)
        print(c("ps",ps,"f_S",f_S,"f_N",f_N))
          for(b in unique(result_baseline$beta)){
        beta<-b
        init.state<-initialcondition(distrsw,distrpop,result_baseline,b,ntype_el,types_el,nagegroups,taux_infec)
        theta<-c("epsilon"=epsilon,"nu"=nu,"eta"=eta,"rho"=rho,"vacc"=vacc,"imm"=imm,"beta"=beta,"p"=p,"typevacc"=typevacc, "q"=q,"f_N"=f_N,"f_S"=f_S,"price"=price,"feed"=feed,"vacc_cost"=vacc_cost,"Q"=Q,"timevacc"=timevacc)
        theta<-c(theta,alphavec)
        theta<-c(theta,mortalityvec)
        theta<-c(theta,incomingvec)
        theta<-c(theta,exploitvec)
        res_2groups<-InfectionDynamique_2(theta,init.state,times)
        res_2groups$beta<-beta
        res_2groups$ps<-ps
        res_2groups$f_N<-f_N
        res_2groups$f_S<-f_S
        res_2groups$Q<-Q
        res_2groups$Year<-as.integer(res_2groups$time/365)
        result_typevacc_1<-rbind(result_typevacc_1,res_2groups)
  
}
    
      }
  }

}
write.csv(result_typevacc_1,"../../3_Outputs/Scenarios_2groups/reuslts_typevacc_1_tentative2.csv")  

```


```{r plotting results}
results<-subset(result_typevacc_1,f_S<1.25 & f_N<1.25 )

  resultsperc<-results[,c("time","ps","f_S","f_N","Z_S","Z_N")]
  resultsperc<- melt(resultsperc, id.vars=c("time","ps","f_S","f_N"))
  aggperc<-aggregate(value ~ time+ps+f_S+f_N+variable, resultsperc,function(x) quantile(x, probs = c(0.025,0.25,0.5,0.75,0.975)))
  aggperc<-do.call(data.frame, aggperc)
  percmean<-aggregate(value ~ time+ps+f_S+f_N+variable, resultsperc,function(x) mean(x))
  aggperc<-merge(aggperc,percmean,by=c("time","ps","f_S","f_N","variable"))
  names(aggperc)<-c("time","ps","f_S","f_N","variable","CI2.5","CI25","Median","CI75","CI97.5","Mean")
  aggperc025<-subset(aggperc,ps==0.25)
  aggperc075<-subset(aggperc,ps==0.75)
  aggperc05<-subset(aggperc,ps==0.5)
  p2<-ggplot(data=aggperc075,aes(x=time,factor=variable,group=variable))
  p2<-p2+geom_line(aes(y=Mean,col=variable),linetype=1)
  p2<-p2+labs(title="Cases")+xlab( paste("Time ( in day)",sep=""))+ylab("Number")
  p2<-p2+geom_ribbon(aes(ymin=CI2.5,ymax=CI97.5,fill=variable),alpha=0.2)
  p2<-p2+facet_grid(vars(f_S),vars(f_N))
  p2<-p2+guides(fill="none")
  p2

```



