
my_posterior <- function(fitmodel=fitmodel,init.state=init.state,theta=theta,theta_fix=theta_fix,limits=limits,times=times,data=data) {

 
  log.sum <-0
  for(p in names(theta)){
    if(limits[["lower"]][[p]]!=limits[["upper"]][[p]]){
      log.p<-dunif(theta[[p]], 1,1, log = TRUE)
      log.sum <-log.sum+ log.p
    }

  }

  log.prior<-log.sum
  # time sequence (must include initial time)
  #times<-seq(0,3650)
  
  # simulate model at successive observation times of data
  result <- fitmodel$simulate(theta,theta_fix,times,init.state)
  
  traj<-result[["traj2"]]
  simulation<-result[["simulation"]]
  dens <- 0
  
  # compute log-likelihood by summing the log-likelihood of each data point
  for(i in 1:(nrow(data))){
    
    # extract data point
    data.point <- unlist(data[i,])
    
    # extract model point
    # we use i+1 since the first row of traj contains the initial state.
    model.point <- unlist(traj[i,])
    
    # update marginal log-likelihood
 
    dens <- dens + fitmodel$dPointObs(data.point=data.point, model.point=model.point, theta=theta,theta_fix=theta_fix, log = TRUE)
    
  }
  newlist<-list("dens"=dens,"simulation"=simulation)
  result<-newlist
  log.likelihood <-result[["dens"]]
  simulation<-result[["simulation"]]
  
  log.posterior <- log.prior + log.likelihood
  
  
  return(list(log.density=log.posterior, trace=c(theta,log.prior=log.prior,log.likelihood=log.likelihood,log.posterior=log.posterior),simulation=simulation))
  
  
}

#' Generate an observation trajectory for a fitmodel
#'

rTrajObs <- function(fitmodel=fitmodel,init.state, theta,theta_fix,data,times) {

  ## simulate model at successive observation times of data
  traj <- fitmodel$simulate(theta,theta_fix,times, init.state)[["traj2"]]
  
  ## generate observations by applying fitmodel$rPointObs to
  ## each row of traj. The parameter value theta as passed as
  ## extra argument to fitmodel$rPointObs
  
  obs <- ddply(traj, "Demo" , fitmodel$rPointObs, theta = theta,theta_fix=theta_fix,data=data)

  traj_obs <- join(traj,obs, by="Demo")
  
  return(traj_obs)
  
  
}



##########################################################

##############################################################################################



mcmcMH_3 <- function(target, fitmodel=fitmodel,times=times,init.theta=init.theta,theta_fix=theta_fix,init.state, proposal.sd = NULL,
                     n.iterations,data, covmat = NULL,
                     limits=limits,
                     adapt.size.start = NULL, adapt.size.cooling = 0.99,
                     adapt.shape.start = NULL, adapt.shape.stop = NULL,
                     print.info.every = n.iterations/100,
                     verbose = FALSE, max.scaling.sd = 50) {
  
  # initialise theta

  init.theta<-init.theta

  theta.current <- init.theta
  theta.propose <- init.theta
  
  # extract theta of gaussian proposal
  covmat.proposal <- covmat
  lower.proposal <- limits$lower
  upper.proposal <- limits$upper
  
  # reorder vector and matrix by names, set to default if necessary
  theta.names <- names(init.theta)
  proposal.sd<-proposal.sd
  if (!is.null(proposal.sd) && is.null(names(proposal.sd))) {
    names(proposal.sd) <- theta.names
  }
  
  if (is.null(covmat.proposal)) {
    if (is.null(proposal.sd)) {
      proposal.sd <- init.theta/10
    }
    covmat.proposal <-
      matrix(diag(proposal.sd[theta.names]^2, nrow = length(theta.names)),
             nrow = length(theta.names),
             dimnames = list(theta.names, theta.names))
  } else {
    covmat.proposal <- covmat.proposal[theta.names,theta.names]
  }
  
  if (is.null(lower.proposal)) {
    lower.proposal <- init.theta
    lower.proposal[] <- -Inf
  } else {
    lower.proposal <- lower.proposal[theta.names]
  }
  
  if (is.null(upper.proposal)) {
    upper.proposal <- init.theta
    upper.proposal[] <- Inf
  } else {
    upper.proposal <- upper.proposal[theta.names]
  }
  #print("done covmat")
  # covmat init
  covmat.proposal.init <- covmat.proposal
  
  adapting.size <- FALSE # will be set to TRUE once we start
  # adapting the size
  
  adapting.shape <- 0  # will be set to the iteration at which
  # adaptation starts
  
  # find estimated theta
  theta.estimated.names <- names(which(diag(covmat.proposal) > 0))
  #print("done estimated.theta")
  #print(theta.estimated.names)
  # evaluate target at theta init

  target.theta.current <- target(fitmodel=fitmodel,init.state=init.state,theta=theta.current,theta_fix=theta_fix,limits=limits,times=times,data=data)
  #print("done target.theta")
  if (!is.null(print.info.every)) {
    message(Sys.time(), ", Init: ", printNamedVector(theta.current[theta.estimated.names]),
            ", target: ", target.theta.current$log.density)
  }

  # trace
  trace <- matrix(ncol=length(theta.current)+1, nrow=n.iterations, 0)
  colnames(trace) <- c(theta.estimated.names, "log.density")
  #print(head(trace))
  # acceptance rate
  acceptance.rate <- 0
  
  # scaling factor for covmat size
  scaling.sd  <- 1
  
  # scaling multiplier
  scaling.multiplier <- 1
  
  # empirical covariance matrix (0 everywhere initially)
  covmat.empirical <- covmat.proposal
  covmat.empirical[,] <- 0
  
  # empirical mean vector
  theta.mean <- theta.current
  
  # if print.info.every is null never print info
  if (is.null(print.info.every)) {
    print.info.every <- n.iterations + 1
  }
  
  start_iteration_time <- Sys.time()
  #print("start iterations")
  for (i.iteration in seq_len(n.iterations)) {
    
    # adaptive step
    if (!is.null(adapt.size.start) && i.iteration >= adapt.size.start &&
        (is.null(adapt.shape.start) || acceptance.rate*i.iteration < adapt.shape.start)) {
      if (!adapting.size) {
        message("\n---> Start adapting size of covariance matrix")
        adapting.size <- TRUE
      }
      # adapt size of covmat until we get enough accepted jumps
      scaling.multiplier <- exp(adapt.size.cooling^(i.iteration-adapt.size.start) * (acceptance.rate - 0.234))
      scaling.sd <- scaling.sd * scaling.multiplier
      scaling.sd <- min(c(scaling.sd,max.scaling.sd))
      # only scale if it doesn't reduce the covariance matrix to 0
      covmat.proposal.new <- scaling.sd^2*covmat.proposal.init
      #print("update covmat")
      if (!(any(diag(covmat.proposal.new)[theta.estimated.names] <
                .Machine$double.eps))) {
        covmat.proposal <- covmat.proposal.new
      }
      
    } else if (!is.null(adapt.shape.start) &&
               acceptance.rate*i.iteration >= adapt.shape.start &&
               (adapting.shape == 0 || is.null(adapt.shape.stop) ||
                i.iteration < adapting.shape + adapt.shape.stop)) {
      if (!adapting.shape) {
        message("\n---> Start adapting shape of covariance matrix")
        # flush.console()
        adapting.shape <- i.iteration
      }
      
      ## adapt shape of covmat using optimal scaling factor for multivariate target distributions
      scaling.sd <- 2.38/sqrt(length(theta.estimated.names))
      
      covmat.proposal <- scaling.sd^2 * covmat.empirical
    } else if (adapting.shape > 0) {
      message("\n---> Stop adapting shape of covariance matrix")
      adapting.shape <- -1
    }
    
    # print info
    if (i.iteration %% ceiling(print.info.every) == 0) {
      message(Sys.time(), ", Iteration: ",i.iteration,"/", n.iterations,
              ", acceptance rate: ",
              sprintf("%.3f",acceptance.rate), appendLF=FALSE)
      if (!is.null(adapt.size.start) || !is.null(adapt.shape.start)) {
        message(", scaling.sd: ", sprintf("%.3f", scaling.sd),
                ", scaling.multiplier: ", sprintf("%.3f", scaling.multiplier),
                appendLF=FALSE)
      }
      message(", state: ",(printNamedVector(theta.current)))
      message(", logdensity: ", target.theta.current$log.density)
    }
    
    # propose another parameter set
    if (any(diag(covmat.proposal)[theta.estimated.names] <
            .Machine$double.eps)) {
      
      stop("non-positive definite covmat",call.=FALSE)
    }
    if (length(theta.estimated.names) > 0) {
      theta.propose[theta.estimated.names] <-
        as.vector(rtmvnorm(1,
                           mean =
                             theta.current[theta.estimated.names],
                           sigma =
                             covmat.proposal[theta.estimated.names,theta.estimated.names],
                           lower =
                             lower.proposal[theta.estimated.names],
                           upper = upper.proposal[theta.estimated.names]))
    }
    
    # evaluate posterior of proposed parameter

    
    target.theta.propose <- target(fitmodel=fitmodel,init.state=init.state,theta=theta.propose,theta_fix=theta_fix,limits=limits,times=times,data=data)


    # if return value is a vector, set log.density and trace
    
    if (!is.finite(target.theta.propose$log.density)) {
      # if posterior is 0 then do not compute anything else and don't accept
      log.acceptance <- -Inf
      
    }else{
      
      # compute Metropolis-Hastings ratio (acceptance probability)
      log.acceptance <- target.theta.propose$log.density - target.theta.current$log.density
      log.acceptance <- log.acceptance +
        dtmvnorm(x = theta.current[theta.estimated.names],
                 mean =
                   theta.propose[theta.estimated.names],
                 sigma =
                   covmat.proposal[theta.estimated.names,
                                   theta.estimated.names],
                 lower =
                   lower.proposal[theta.estimated.names],
                 upper =
                   upper.proposal[theta.estimated.names],
                 log = TRUE)
      log.acceptance <- log.acceptance -
        dtmvnorm(x = theta.propose[theta.estimated.names],
                 mean = theta.current[theta.estimated.names],
                 sigma =
                   covmat.proposal[theta.estimated.names,
                                   theta.estimated.names],
                 lower =
                   lower.proposal[theta.estimated.names],
                 upper =
                   upper.proposal[theta.estimated.names],
                 log = TRUE)
      
    }
    
    if (verbose) {
      message("Propose: ", theta.propose[theta.estimated.names],
              ", target: ", target.theta.propose,
              ", acc prob: ", exp(log.acceptance), ", ",
              appendLF = FALSE)
    }
    
    if (is.accepted <- (log(runif (1)) < log.acceptance)) {
      # accept proposed parameter set
      theta.current <- theta.propose
      target.theta.current <- target.theta.propose
      if (verbose) {
        message("accepted")
      }
    } else if (verbose) {
      message("rejected")
    }
    if(is.finite(target.theta.current$log.density)){
      trace[i.iteration, ] <- c(theta.current, target.theta.current$log.density)
    }
    
    
    # update acceptance rate
    if (i.iteration == 1) {
      acceptance.rate <- is.accepted
    } else {
      acceptance.rate <- acceptance.rate +
        (is.accepted - acceptance.rate) / i.iteration
    }
    
    # update empirical covariance matrix
    if (adapting.shape >= 0) {
      tmp <- updateCovmat(covmat.empirical, theta.mean,
                          theta.current, i.iteration)
      covmat.empirical <- tmp$covmat
      theta.mean <- tmp$theta.mean
    }
    
  }
  
  return(list(trace = trace,
              acceptance.rate = acceptance.rate,
              covmat.empirical = covmat.empirical))
}
###############################################################################################
