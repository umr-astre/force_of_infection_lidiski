---
title: "FOI_Nimble"
author: "Andrea Apolloni"
date: "2023-02-20"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
#knitr::root.dir("/Users/apolloni/Documents/Projects/PPR_Nigeria/force_of_infection_lidiski/2_Scripts/")
```

```{r packages,warning=FALSE}
library(readxl)
library(writexl)
library(reshape2)
library(ggplot2)
library(lme4)
library(dplyr)
library(tidyr)
library(brms)
library(ggmcmc)
library(mcmcplots)
library(nimble)
library(postpack)
library(gridExtra)
library(cowplot)

```



```{r functions to be used,echo=FALSE,warning=FALSE}

#### Function used to predict the seroprevalence by age-class afteribration

pfunc<-function(l0, l1,a,age0,agelamb){
  p<- (1-exp(-l0*(agelamb-age0))*exp(-l1*(a-agelamb)))
  return(p)
}

simSeroFunc_mortality <- function(l0, l1,pm0,pm1,a,age0,agelamb,pos,neg){
   p<-pfunc(l0, l1,a,age0,agelamb)
   tot<-pos+neg
   if(a>agelamb){
     pos<-rbinom(1,tot,(1-pm1)*p)
   }else{
     pos<-rbinom(1,tot,(1-pm0)*p)
   }
   
   return(pos)
 }  




#### par= samplign parameters
mortagefun_mort<-function(par,df){
  l0<-par[1]
  l1<-par[2]
  pm0<-par[3]
  pm1<-par[4]
  agevector<-df$age_month
  age0<-df$offset[1]
  tot<-df$meanR+df$meanD
  pmvector<-c(pm0,pm1,pm1,pm1)
  res<-c()
  for (i in 1:length(agevector) ) {
# True occupancy status
    dt<-rbinom(1,tot[i],1-(1-pmvector[i])^12)
    res<-c(res,dt)
  }
  return(res)
}


seroagefun_mort<-function(par,df){
  l0<-par[1]
  l1<-par[2]
  pm0<-par[3]
  pm1<-par[4]
  agevector<-df$age_month
  age0<-df$offset[1]
  tot<-df$Pos+df$Neg
  pmvector<-c(pm0,pm1,pm1,pm1)
  res<-c()
  for (i in 1:length(agevector) ) {
    a<-agevector[i]
    agelamb<-agevector[1]
    p <- pfunc(l0, l1,a,age0,agelamb)# True occupancy status
    pos<-rbinom(1,tot[i],(1-pmvector[i])*p)
    res<-c(res,pos)
  }
  return(res)
}

plotsim_mort<-function(PlateauSero,PlateauMort,samplinglist){
  plotdata<-as.data.frame((apply(samplinglist,1,function(x) seroagefun_mort(x,PlateauSero))))
plotdata_agg<- plotdata %>%
  rowwise() %>% 
  summarise(mean = mean(c_across()),sd=sd(c_across()))

plotdata_agg<-cbind(plotdata_agg,PlateauSero)

  plotmort<-as.data.frame((apply(samplinglist,1,function(x) mortagefun_mort(x,PlateauMort))))
plotmort_agg<- plotmort %>%
  rowwise() %>% 
  summarise(mean = mean(c_across()),sd=sd(c_across()))

plotmort_agg<-cbind(plotmort_agg,PlateauMort)


Plateauplotbayes<-ggplot(plotdata_agg,aes(x=age_month,y=Perc))+geom_point(col="black")+geom_point(aes(y=100*mean/(Pos+Neg)),col="red")+geom_errorbar(aes(ymin=100*mean/(Pos+Neg)-100*sd/(Pos+Neg),ymax=100*mean/(Pos+Neg)+100*sd/(Pos+Neg)),col="red")+scale_x_continuous(name ="Age class",breaks=plotdata_agg$age_month, labels=plotdata_agg$age)+scale_y_continuous(name="Percentage(%)",breaks=seq(0,100,10),limits=c(00,100))

Plateauplotbayes_mort<-ggplot(plotmort_agg,aes(x=age_month,y=100*meanD/(meanR+meanD)))+geom_point(col="black")+geom_point(aes(y=100*mean/(meanR+meanD)),col="red")+geom_errorbar(aes(ymin=100*mean/(meanR+meanD)-100*sd/(meanR+meanD),ymax=100*mean/(meanR+meanD)+100*sd/(meanR+meanD)),col="red")+scale_x_continuous(name ="Age class",breaks=plotmort_agg$age_month, labels=plotmort_agg$age)+scale_y_continuous(name="Percentage(%)",breaks=seq(0,100,10),limits=c(00,100))
Plotfin<-plot_grid(Plateauplotbayes, Plateauplotbayes_mort, ncol=2,labels = c("Seroprevalence","Case fatality"))
return(Plotfin)
}

plotsimLGA<-function(PlateauLGASero,samplingpar){
  df3<-merge(PlateauLGASero,samplingpar,by="LGA")
  df3$agelamb<-agelamb
  df3$res<-apply(df3[,c('l0','l1','age_month','offset','agelamb',"Pos","Neg")], 1, function(y) simSeroFunc(y['l0'],y['l1'],y['age_month'],y["offset"],y["agelamb"],y["Pos"],y["Neg"]))
plotdata_agg<- df3 %>%
  group_by(LGA,age,age_month)%>%
  summarise(Pos=mean(Pos),Neg=mean(Neg),Perc=mean(Perc),mean = mean(res),sd=sd(res),Low95 =quantile(res,probs=.025),Up95 =quantile(res,probs=.975),Low50 =quantile(res,probs=.25),Up50 =quantile(res,probs=.75))


Plateauplotbayes<-ggplot(plotdata_agg,aes(x=age_month,y=Perc,group=LGA))+geom_point(col="black")+geom_point(aes(y=100*mean/(Pos+Neg)),col="red")+geom_ribbon(aes(ymin=100*Low95/(Pos+Neg),ymax=100*Up95/(Pos+Neg)),fill="red",alpha=0.1)+geom_ribbon(aes(ymin=100*Low50/(Pos+Neg),ymax=100*Up50/(Pos+Neg)),fill="red",alpha=0.25)+scale_x_continuous(name ="Age class",breaks=plotdata_agg$age_month, labels=plotdata_agg$age)+scale_y_continuous(name="Percentage(%)",breaks=seq(0,100,10),limits=c(0,100))+facet_wrap(~LGA)

return(Plateauplotbayes)
}
```





# Introduction

Aim of this markdown is to estimate the force of infection for Bauchi and Plateau using  a bayesian approach. To this end we use the nimble package to handle the MCMC simulations. Compared  to the previous case we consider that  Small ruminants could die due to infection from PPR



## Catalytic model and force of infection without mortality

The force of infection $\lambda$  represents the probability for a susceptible individual to get infected in a specific interval of time. In the case of an endemic diseases , like the case of PPR , the f.o.i. can be considered constant.

The catalytic model is a  model  developed to estimate the force of infection given the sero-prevalence profile in a population. Given a population divide in age groups , indicating with $s(a)$  the fraction of susceptible individuals in each class of age (a) who haven't experienced the disease yet, and with $z(a)$ the fraction of individuals that already have experienced the disease (and they are seropositives), the dynamical model can be written  as:

\begin{eqnarray}
\frac{ds}{d a}&=& -\lambda s(a) \\
\frac{dz}{d a}&=& \lambda s(a)
\end{eqnarray}

The model simply considers the flow from one state to the other along the years of life of the animals 

The model can be solved as  

$$
s(a) = e^{-\lambda a} \\
z(a) = 1- e^{-\lambda a}
$$

The term $ \Pi(a)= 1- e^{-\lambda a}$ corresponds to the probability of finding a positive animals of age (a). The probability of finding  positive animals increases with age , since animals are more exposed , however the curve tend to flatten , and increases slower at larger age.

The model assumes that the probability of death  by the disease is negligible compared to natural death.  

The model can be extended to consider 2 elements that will be taken account in this analysis:
  
* The presence of maternal antibody: Animals born from recovered mothers are born with maternal antibodies and could be protected by the disease for a certain ("offset") period of time. Furthermore during this period the seroprevalence would be elevated in the the newborn group. Because of this an offset period is introduced in the above formula 

$$ 
\Pi(a)=1-e^{-\lambda(a-offset)}
$$
* The force of infection can change  with the age , due to different level of exposure to the disease. It's common knowledge that most of the PPR infections occur among young (lambs) animals, while infections are seldom among older animals. Because of this 2 force of infections $\lambda$s will be introduced for the animals of age below and above a certain age ($age_l$) and the above formula becomes:

$$ 
\Pi(a)=1-e^{-\lambda_0 (a-offset)} \text{ when } a\leq age_l \\
\Pi(a)=1-e^{-\lambda_0 (age_l-offset)}e^{-\lambda_1 (a-age_l)}  \text{ when } a> age_l 
$$

4 age classes were used in the data collection: lamb (3-12 months); Young(12-24);Mature (24-36); Old (>36). In our analysis we use the center age of the interval and we suppose an age max of 4 years for the old one. The offset was put at 3 months

```{r defining ages}
agelamb<-7.5
ageyoung<-18
agemature<-30
ageold<-42
offset_month<-3
```
In our case we are going to estimate the  force of infection for the lambs and the rest of the animals by calibrating a catalytic model on the number of seropositive animals. We use a Bayesian approach and we estimate the  distribution function of $\lambda_0,\lambda_1$ using as likelihood a binomial one.We are using the data collected in Bauchi and Plateau  to inform the  model using the same age group defined by the teams. 

We initially consider the total number of positive and sampled animals at State level. __In this model the force of infection will provide the probability per month  and age group of being infected__ 

In a second step, to take account of geographical differences , we develop a hierarchical model and estimate the force of infection by age and LGA


### Adding mortality to catalytic model

In the previous section we have considered that the population is not-mortal, i.e. all animals infected , recovered after the infection. Nevertheless depending on age , animals can die  with probability $m_a$ or recovered with probability $1- m_a$. Indicating with $d(a)$ the number of animals dead after the infection and with $r(a)$ the recovered ones , we can rewrite the  above relations as :


$$
s(a) = e^{-\lambda a} \\
z(a) = d(a)+r(a)\\
d(a) = m_a*z(a)=m_a*(1-e^{-\lambda a})\\
r(a) = (1-m_a)*z(a)=(1-m_a)*(1-e^{-\lambda a})
$$

Serological data collected in the 2 States contain information only on the surviving population and not about the dead ones that are collected from other data (proportional piling). To take account of the mortality and re-estimate  the f.o.i. the  sampled population should be rescaled:

$$
n(a) = s(a)+r(a)\\
n'(a) = s(a)+r(a)+d(a)=s(a)+r(a)+\frac{m_a}{(1-m_a)}r(a)\\
n'(a) = n(a)+\frac{m_a}{(1-m_a)}r(a)
$$
In our model we consider for each LGA a 2 levels mortality rates for

$$ 
m_0 \text{ when } a\leq age_l \\
m_1 \leq m_0 \text{ when } a> age_l 
$$
where $age_l$ is the same age limit as the previous case . We use data from the proportional piling (given for young and adult cases). The Risk  Ratio is estimated for each age-group and location as the ratio between the recovered ($1-m_a$) and dead one. The R.R. is supposed to follow lognormal distribution

## Importing data

We use serological data collected in Bauchi and Plateau for estimating the FOI and the proportiaol piling for the mortality one in the same LGA. We created 2 dataframes for the serology (one for Bauchi and for Plateau ) where  for each State we considered only the number of Positive and Negative by Class Age and 2 Dataframes for the mortality , where for each state and age group we considered the ratio between recovered and dead. 

```{r importing data}
Plateau<-read.table("../1_Data/avh.txt")
Bauchi<-read.csv("../1_Data/avh.csv",sep=";")
GoatDisease<-read.csv("../1_Data/Disease.effect.goat.csv",sep=";")
GoatDisease<-GoatDisease[,c(1:16)]
SheepDisease<-read.csv("../1_Data/Disease.effect.sheep.csv",sep=";")
SRDisease<-rbind(GoatDisease,SheepDisease)
PlateauSRD<-subset(SRDisease,State=="Plateau")
BauchiSRD<-subset(SRDisease,State=="Bauchi")
```

To the dataframes we add the value in month of the ages and we add the offset  month for further use


```{r creating datasets}
PlateauSero<-Plateau %>% 
  filter(Herd_vax=="no")%>%
  group_by(age,animal_spe,gender) %>%  
  dplyr::summarize(Pos = sum(ELISA_statusb=="positive",na.rm=TRUE),Neg = sum(ELISA_statusb=="negative",na.rm=TRUE))%>%
  mutate(Perc=100*Pos/(Neg+Pos))%>%
  mutate(
   age_month = case_when(age == "lamb" ~ agelamb,
                    age == "young" ~ ageyoung,
                    age == "mature" ~ agemature,
                    age == "old" ~ ageold)
  )%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)
PlateauSero$offset<-offset_month

  BauchiSero<-Bauchi %>% 
    filter(Herd_vax=="no")%>%
    group_by(age,animal_spe,gender) %>%  
  dplyr::summarize(Pos = sum(ELISA_status=="positive",na.rm=TRUE),Neg = sum(ELISA_status=="negative",na.rm=TRUE))%>%
  mutate(Perc=100*Pos/(Neg+Pos))%>%
  mutate(
   age_month = case_when(age == "lamb" ~ agelamb,
                    age == "young" ~ ageyoung,
                    age == "mature" ~ agemature,
                    age == "old" ~ ageold)
  )%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)
BauchiSero$offset<-offset_month
```


```{r creating datasets mortaltiy}
PlateauMort<-PlateauSRD %>% 
  mutate(RR_Y=Young.dead/Young.recovered,RR_A=Adult.dead/Adult.recovered)%>%
  mutate(lamb=RR_Y,young=RR_A,mature=RR_A,old=RR_A)%>%
  select(Village.ID,State,LGA,District,Village.name,Sex,lamb,young,mature,old)%>%
  gather("age","RR","lamb":"old")%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)%>%
  rowwise %>% 
  filter(!any(is.infinite(c_across(where(is.numeric)))))%>%
  group_by(age,Sex)%>%
  summarize(mRR=mean(RR,na.rm=TRUE),sdRR=sd(RR,na.rm=TRUE))%>%
  mutate(
   age_month = case_when(age == "lamb" ~ agelamb,
                    age == "young" ~ ageyoung,
                    age == "mature" ~ agemature,
                    age == "old" ~ ageold)
  )

PlateauMort$offset<-offset_month

PlateauMort2<-PlateauSRD %>% 
  mutate(R_Y=Young.recovered,R_A=Adult.recovered)%>%
  mutate(R_lamb=R_Y,R_young=R_A,R_mature=R_A,R_old=R_A)%>%
  select(Village.ID,State,LGA,District,Village.name,Sex,R_lamb,R_young,R_mature,R_old)%>%
  gather(age, Recovered, starts_with("R_"))%>%
  mutate(age=stringr::str_remove(age, "R_"))%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)%>%
  rowwise %>% 
  filter(!any(is.infinite(c_across(where(is.numeric)))))%>%
  group_by(age,Sex)%>%
  summarize(meanR=round(mean(Recovered,na.rm=TRUE),0))


PlateauMort3<-PlateauSRD %>% 
  mutate(D_Y=Young.dead,D_A=Adult.dead)%>%
  mutate(D_lamb=D_Y,D_young=D_A,D_mature=D_A,D_old=D_A)%>%
  select(Village.ID,State,LGA,District,Village.name,Sex,D_lamb,D_young,D_mature,D_old)%>%
  gather(age, Deaths, starts_with("D_"))%>%
  mutate(age=stringr::str_remove(age, "D_"))%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)%>%
  rowwise %>% 
  filter(!any(is.infinite(c_across(where(is.numeric)))))%>%
  group_by(age,Sex)%>%
  summarize(meanD=round(mean(Deaths,na.rm=TRUE),0))


PlateauMort<-merge(PlateauMort,PlateauMort2,by=c("age","Sex"))
PlateauMort<-merge(PlateauMort,PlateauMort3,by=c("age","Sex"))
PlateauMort<-PlateauMort%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)

#########################################################"

BauchiMort<-BauchiSRD %>% 
  mutate(RR_Y=Young.dead/Young.recovered,RR_A=Adult.dead/Adult.recovered)%>%
  mutate(lamb=RR_Y,young=RR_A,mature=RR_A,old=RR_A)%>%
  select(Village.ID,State,LGA,District,Village.name,Sex,lamb,young,mature,old)%>%
  gather("age","RR","lamb":"old")%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)%>%
  rowwise %>% 
  filter(!any(is.infinite(c_across(where(is.numeric)))))%>%
  group_by(age,Sex)%>%
  summarize(mRR=mean(RR,na.rm=TRUE),sdRR=sd(RR,na.rm=TRUE))%>%
  mutate(
   age_month = case_when(age == "lamb" ~ agelamb,
                    age == "young" ~ ageyoung,
                    age == "mature" ~ agemature,
                    age == "old" ~ ageold)
  )

BauchiMort$offset<-offset_month

BauchiMort2<-BauchiSRD %>% 
  mutate(R_Y=Young.recovered,R_A=Adult.recovered)%>%
  mutate(R_lamb=R_Y,R_young=R_A,R_mature=R_A,R_old=R_A)%>%
  select(Village.ID,State,LGA,District,Village.name,Sex,R_lamb,R_young,R_mature,R_old)%>%
  gather(age, Recovered, starts_with("R_"))%>%
  mutate(age=stringr::str_remove(age, "R_"))%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)%>%
  rowwise %>% 
  filter(!any(is.infinite(c_across(where(is.numeric)))))%>%
  group_by(age,Sex)%>%
  summarize(meanR=round(mean(Recovered,na.rm=TRUE),0))


BauchiMort3<-BauchiSRD %>% 
  mutate(D_Y=Young.dead,D_A=Adult.dead)%>%
  mutate(D_lamb=D_Y,D_young=D_A,D_mature=D_A,D_old=D_A)%>%
  select(Village.ID,State,LGA,District,Village.name,Sex,D_lamb,D_young,D_mature,D_old)%>%
  gather(age, Deaths, starts_with("D_"))%>%
  mutate(age=stringr::str_remove(age, "D_"))%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)%>%
  rowwise %>% 
  filter(!any(is.infinite(c_across(where(is.numeric)))))%>%
  group_by(age,Sex)%>%
  summarize(meanD=round(mean(Deaths,na.rm=TRUE),0))


BauchiMort<-merge(BauchiMort,BauchiMort2,by=c("age","Sex"))
BauchiMort<-merge(BauchiMort,BauchiMort3,by=c("age","Sex"))
BauchiMort<-BauchiMort%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)


```
At first we consider the National seroprevalence without distinction by sex ,species or LGA. 



## Bayesian approach using nimble

### Plateau
#### State level FOI with offset nimble
We define the Code for simulating the catalytic model. The model consider that during the first age0 months (corresponding to agelamb), the force of infection (l0) is different from the other  months (l1) and there is an offset of age0 months 
```{r Plateau nimble def model,warning=FALSE}

seroCodeMortality <- nimbleCode({
  l0 ~ dunif(0,1)
  l1 ~ dunif(0,1)
  pm0 ~ dunif(0.01,0.91)#dnorm(mean=RR0/(1+RR0),sd=0.5)
  pm1 ~ dunif(0.01,0.91)#dnorm(mean=RR1/(1+RR1),sd=0.5)

  pmvector[1]<-pm0
  pmvector[2]<-pm1
  pmvector[3]<-pm1
  pmvector[4]<-pm1
  for (i in 1:4 ) {
    a[i]<-agevector[i]
    p[i] <- (1-exp(-l0*(agelamb-age0))*exp(-l1*(a[i]-agelamb))) # True occupancy status
    totm[i]<-tot[i]+round(pos2[i]*(pm0)/(1-pm0))
    pos[i]~dbinom((1-pmvector[i])*p[i],totm[i])
   
    
  }
  for (i in 1:2 ) {
  deaths[i]~dbinom((1-pmvector[i])^12,(exposed[i]))
  }
})
```

List of parameters  and input factors to calibrate the model with

```{r Plateau Male Nimble parameters}
PlateauSeroGoatmale<-subset(PlateauSero,gender=="male" )
PlateauMortmale<-subset(PlateauMort,Sex=="M")
seroConsts<-list(agevector=PlateauSeroGoatmale$age_month,age0=offset_month,agelamb=agelamb)

seroData<-list(pos=PlateauSeroGoatmale$Pos,pos2=PlateauSeroGoatmale$Pos,tot=PlateauSeroGoatmale$Pos+PlateauSeroGoatmale$Neg,deaths=PlateauMortmale$meanD,recovered=PlateauMortmale$meanR,exposed=PlateauMortmale$meanR+PlateauMortmale$meanD)
pmvector<- nimArray(0, dim=4)
pmvector[1]<-0.01
pmvector[2]<-0.01
pmvector[3]<-0.01
pmvector[4]<-0.01
seroInits<-list(l0=0.01,l1=0.02,pmvector=pmvector)
seroModel <- nimbleModel(code = seroCodeMortality, name = 'SerologyMort', constants = seroConsts,data = seroData, inits = seroInits)

seroMCMC <- buildMCMC(seroModel)
CseroModel <- compileNimble(seroModel,showCompilerOutput = TRUE)
CseroMCMC <- compileNimble(seroMCMC, project = seroModel)
```


```{r Plateau  Male running model}
runMCMC_samples <- runMCMC(CseroMCMC, nburnin = 2000,thin=10, niter = 40000,nchains = 10)

PlateauMaleGoatcodalist<-post_convert(runMCMC_samples)
summary(PlateauMaleGoatcodalist)
```


```{r Plateau Female Nimble parameters}
PlateauSeroGoatfemale<-subset(PlateauSero,gender=="female" )
PlateauMortfemale<-subset(PlateauMort,Sex=="F")
seroConsts<-list(agevector=PlateauSeroGoatfemale$age_month,age0=offset_month,agelamb=agelamb)

seroData<-list(pos=PlateauSeroGoatfemale$Pos,pos2=PlateauSeroGoatfemale$Pos,tot=PlateauSeroGoatfemale$Pos+PlateauSeroGoatfemale$Neg,deaths=PlateauMortfemale$meanD,recovered=PlateauMortfemale$meanR,exposed=PlateauMortfemale$meanR+PlateauMortfemale$meanD)
pmvector<- nimArray(0, dim=4)
pmvector[1]<-0.01
pmvector[2]<-0.01
pmvector[3]<-0.01
pmvector[4]<-0.01
seroInits<-list(l0=0.01,l1=0.02,pmvector=pmvector)
seroModel <- nimbleModel(code = seroCodeMortality, name = 'SerologyMort', constants = seroConsts,data = seroData, inits = seroInits)

seroMCMC <- buildMCMC(seroModel)
CseroModel <- compileNimble(seroModel,showCompilerOutput = TRUE)
CseroMCMC <- compileNimble(seroMCMC, project = seroModel)
```


```{r Plateau  Fefemale running model}
runMCMC_samples <- runMCMC(CseroMCMC, nburnin = 2000,thin=10, niter = 40000,nchains = 10)

PlateauFemaleGoatcodalist<-post_convert(runMCMC_samples)
summary(PlateauFemaleGoatcodalist)
```

We check if the chains converge using both the Gelman-Rubin and Geweke plots and tests

In the case of the Gelman-Rubin we check if the ratio between the Upper CI/Upper limit of the PRSF file is closed to 1 (Rhat)


```{r Plateau checking diagnostic}
gelman.diag(PlateauMaleGoatcodalist[, 1:4])
gelman.plot(PlateauMaleGoatcodalist[, 1:4])
```

The Geweke Diagnostic shows the z-scores for a test of equality of means between the first and last parts of each chain, which should be <1.96. 


```{r convergence Plateau model geweke plot}
geweke.diag(PlateauMaleGoatcodalist[, 1:4])
geweke.plot(PlateauMaleGoatcodalist[, 1:4])
```

```{r Plateau plot paramters distribution}

plot(PlateauMaleGoatcodalist[,1:2])

plot(PlateauMaleGoatcodalist[,3:4])
```

Plotting the seroprevalence estimated

```{r Plateau seroprevalence estimated}
nsample<-1000
samplinglist<-data.frame()
for(l in 1:length(runMCMC_samples)){
  samplinglist<-rbind(samplinglist,runMCMC_samples[[l]][sample(nrow(runMCMC_samples[[l]]),size=nsample,replace=TRUE),])
}

SeroPlateauPlot<-plotsim_mort(PlateauSeroGoatmale,PlateauMortmale,samplinglist)
SeroPlateauPlot
```

In the plot the red errorbar shows the distribution of the simulated seroprevalence by ageclass , and the black dots the seroprevalence data


#### Plateau with hierarchical model

We consider that for each LGA the forces of infection $\lambda_0,\lambda_1$ are generated from a distribution of hyperparameters: one distribution for $l_0$ and one fo $l_1$. This is a "compromise" solution that allows some variability of the force of infection among the different LGAs acknowledginga t the same time  diferrences  (variability of $\lambda$s and some similarity (values extracted from the same distribution))
Initially we redefine the model and the dataframe for the calibration . In this case we calibrate the model on the seroprevalence by age in each LGA

```{r Plateau hierachical defining model,warning=FALSE, message=FALSE}

PlateauLGASero<-Plateau %>% group_by(age,LGA) %>%  
  dplyr::summarize(Pos = sum(ELISA_statusb=="positive",na.rm=TRUE),Neg = sum(ELISA_statusb=="negative",na.rm=TRUE))%>%
  mutate(Perc=100*Pos/(Neg+Pos))%>%
  mutate(
   age_month = case_when(age == "lamb" ~ agelamb,
                    age == "young" ~ ageyoung,
                    age == "mature" ~ agemature,
                    age == "old" ~ ageold)
  )%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)
PlateauLGASero<-PlateauLGASero[!is.na(PlateauLGASero$LGA),]
PlateauLGASero$offset<-offset_month




hierarcLGACode <- nimbleCode({
  for(i in  1:N) {
    
    # True occupancy status
    pos[i]~dbinom(p[i],tot[i])
  }
  # mixture component parameters drawn from base measures
  
  for(l in 1:Nunique){
    l1[l] ~ dbeta(a1, b1)
    l0[l] ~ dbeta(a0, b0)
  }
  
  for (i in 1:N){
    
    
    age[i]<-agevector[i]

    p[i] <- (1-exp(-l0[LGAs[i]]*(agelamb-age0))*exp(-l1[LGAs[i]]*(age[i]-agelamb)))
    
  }
  # hyperparameters Lambda1
  a1 <- mu1*eta1
  b1 <- (1-mu1)*eta1
  mu1 ~ dbeta(mua1, mub1)
  eta1 <- exp(logeta1)
  logeta1 ~ dlogis(logn1, 1)# hyperparameters Lambda1
  # hyperparameters Lambda0
  a0 <- mu0*eta0
  b0 <- (1-mu0)*eta0
  mu0 ~ dbeta(mua0, mub0)
  eta0 <- exp(logeta0)
  logeta0 ~ dlogis(logn0, 1)# hyperparameters Lambda0
})

listLGA<-unique(PlateauLGASero$LGA)
PlateauLGASero$LGAn<-match(PlateauLGASero$LGA,listLGA)
seroConsts<-list(agevector=PlateauLGASero$age_month,N=length(PlateauLGASero$LGA),age0=PlateauLGASero$offset[1],agelamb=agelamb,LGAs=(PlateauLGASero$LGAn),Nunique=length(unique(PlateauLGASero$LGA)))

seroData<-list(pos=PlateauLGASero$Pos,tot=PlateauLGASero$Pos+PlateauLGASero$Neg)
seroInits<-list(mua1 = 1, mub1 = 1, logn1 = log(100),mua0 = 1, mub0 = 1, logn0 = log(100))

serohModel <- nimbleModel(code = hierarcLGACode, name = 'Serology LGA', constants = seroConsts,
                          data = seroData, inits = seroInits)

#serohMCMC <- buildMCMC(serohModel)
CserohModel <- compileNimble(serohModel)
#CserohMCMC <- compileNimble(serohMCMC, project = serohModel)
conf <- configureMCMC(serohModel, monitors = c('l0','l1'), print = TRUE)
mcmc <- buildMCMC(conf)
cmcmc <- compileNimble(mcmc, project = serohModel)

runMCMC_samples <- runMCMC(cmcmc, nburnin = 2000, niter = 50000,nchains = 10,thin=10)

PlateauLGAcodalist<-post_convert(runMCMC_samples)
```
As in the previous case we chack the diagnostic of the algorithm and the Gelman diag gives a value very close to 1

```{r Plateau LGA checking diagnostic}
gelman.diag(PlateauLGAcodalist)
#gelman.plot(PlateauLGAcodalist)
```

We summarise the geweke diag to understand if there are chains that are not stationary. To this end we check if for any of the `r length(PlateauLGAcodalist)`  chains, at least for one of the 2 lambdas for each LGA , the geweke value is higher of 1.96. In case it happens we remove the corresponding chain from our analysis

```{r Plateau LGA geweke test}
gew<-geweke.diag(PlateauLGAcodalist)
count_notconverging<-0
removing<-c()
for(i in 1:length(gew)){
  zlist<-gew[[i]]$z
  if(any(abs(zlist)>1.96)){
    count_notconverging<-count_notconverging+1
    removing<-c(removing,i)
  }
}
runMCMC_samplesred<-runMCMC_samples[-removing]
PlateauLGAcodalist<-post_convert(runMCMC_samplesred)
```

A total of `r count_notconverging` chains over `r length(runMCMC_samples)` simulated chains have been found non-stationary and removed

Plotting the parameters estimated from  the model

```{r Plateau LGA hierarchy parameters}
summarypar<-data.frame()
for(l in 1:length(runMCMC_samples)){
  summarypar<-rbind(summarypar,runMCMC_samples[[l]])
}
summaryl0<-summarypar[,1:17]
names(summaryl0)<-listLGA
summaryl0_LGA<-melt(summaryl0,variable.name="LGA",value.name="l0")

summaryl1<-summarypar[,18:34]
names(summaryl1)<-listLGA
summaryl1_LGA<-melt(summaryl1,variable.name="LGA",value.name="l1")
summarypar<-cbind(summaryl0_LGA,summaryl1_LGA)
summarypar<-summarypar[,c(1,2,4)]

summaryparm<-melt(summarypar,id.vars="LGA",value.name = "value",variable.name="Lambda")

summarystate<-data.frame()
for(l in 1:length(Plateaucodalist)){
  summarystate<-rbind(summarystate,Plateaucodalist[[l]])
}
summarystate$LGA<-"State-level"
summarystatem<-melt(summarystate,id.vars="LGA",value.name = "value",variable.name="Lambda")

summaryparm<-rbind(summaryparm,summarystatem)
PlateauLGAsummaryparmplot<-ggplot(summaryparm, aes(x=LGA, y=value, fill=Lambda)) + 
    geom_boxplot()+coord_flip()
PlateauLGAsummaryparmplot
```

Estimates for the parameters are saved in a csv files for further use . The table contains also the estimates at state level

```{r Plateau LGA hierachy summary value,warning=FALSE}


PlateauLGAsummarypartable<-summaryparm %>%
  group_by(LGA,Lambda)%>%
  summarise(mean = mean(value),sd=sd(value),Low95 =quantile(value,probs=.025),Low50 =quantile(value,probs=.25),Up50 =quantile(value,probs=.75),Up95 =quantile(value,probs=.975))

write.csv(PlateauLGAsummarypartable,"../3_Outputs/hierachical_parameters_Plateau_v2.csv")
```


Plotting the seroprevalence estimated. In red the simulated seroprevalecne by ageclass by LGA . The shaded aea correspond to 50 and 95% C.I. 
```{r Plateau hierarchical model seroprevalence estimated}
nsample<-2000
samplinglist<-data.frame()
for(l in 1:length(runMCMC_samples)){
  samplinglist<-rbind(samplinglist,runMCMC_samples[[l]][sample(nrow(runMCMC_samples[[l]]),size=nsample,replace=TRUE),])
}
samplingl0<-samplinglist[,1:17]
names(samplingl0)<-listLGA
samplingl0_LGA<-melt(samplingl0,variable.name="LGA",value.name="l0")

samplingl1<-samplinglist[,18:34]
names(samplingl1)<-listLGA
samplingl1_LGA<-melt(samplingl1,variable.name="LGA",value.name="l1")
samplingpar<-cbind(samplingl0_LGA,samplingl1_LGA)
samplingpar<-samplingpar[,c(1,2,4)]
SeroLGAPlateauPlot<-plotsimLGA(PlateauLGASero,samplingpar)
SeroLGAPlateauPlot
```

### Bauchi

#### State level FOI with offset nimble

We define the Code for simulating the catalytic model. The model consider that during the first age0 months (corresponding to agelamb), the force of infection (l0) is different from the other  months (l1) and there is an offset of age0 months T


```{r Bauchi nimble def model}

seroCode <- nimbleCode({
  for (i in 1:length(agevector) ) {
    a[i]<-agevector[i]
    p[i] <- (1-exp(-l0*(agelamb-age0))*exp(-l1*(a[i]-agelamb))) # True occupancy status
    pos[i]~dbinom(p[i],tot[i])
  }
  l0 ~ dunif(0,1)
  l1 ~ dunif(0,1)
})
```

List of parameters  and input factors to calibrate the model with

```{r Bauchi Nimble parameters,warning=FALSE,message=FALSE}
seroConsts<-list(agevector=BauchiSero$age_month,age0=offset_month,agelamb=agelamb)

seroData<-list(pos=BauchiSero$Pos,tot=BauchiSero$Pos+BauchiSero$Neg)
seroInits<-list(l0=0.01,l1=0.02)

seroModel <- nimbleModel(code = seroCode, name = 'Serology', constants = seroConsts,
                         data = seroData, inits = seroInits)

seroMCMC <- buildMCMC(seroModel)
CseroModel <- compileNimble(seroModel)
CseroMCMC <- compileNimble(seroMCMC, project = seroModel)
```

```{r Bauchi running model}
runMCMC_samples <- runMCMC(CseroMCMC, nburnin = 2000,thin=10, niter = 40000,nchains = 10)

Bauchicodalist<-post_convert(runMCMC_samples)
summary(Bauchicodalist)
```
We check if the chains converge using both the Gelman-Rubin and Geweke plots and tests

In the case of the Gelman-Rubin we check if the ratio between the Upper CI/Upper limit of the PRSF file is closed to 1 (Rhat)


```{r Bauchi checking diagnostic}
gelman.diag(Bauchicodalist[, 1:2])
gelman.plot(Bauchicodalist[, 1:2])
```

The Geweke Diagnostic shows the z-scores for a test of equality of means between the first and last parts of each chain, which should be <1.96. 


```{r convergence Bauchi model geweke plot}

geweke.diag(Bauchicodalist[, 1:2])
#geweke.plot(Bauchicodalist[, 1:2])
```

```{r Bauchi plot paramters distribution}
plot(Bauchicodalist)
```

Plotting the seroprevalence estimated

```{r Bauchi seroprevalence estimated}
nsample<-200
samplinglist<-data.frame()
for(l in 1:length(runMCMC_samples)){
  samplinglist<-rbind(samplinglist,runMCMC_samples[[l]][sample(nrow(runMCMC_samples[[l]]),size=nsample,replace=TRUE),])
}

SeroBauchiPlot<-plotsim(BauchiSero,samplinglist)
SeroBauchiPlot
```

As in the previous case , the red errorbar contains the simulation of seroprevalence by class age  with standard deviation. The agreement between the model and the data is satistfying for older groups


#### Bauchi with hierarchical model

We consider that for each LGA the forces of infection $\lambda_0,\lambda_1$ are generated from a distribution of hyperparameters: one distribution for $l_0$ and one fo $l_1$. This is a "compromise" solution that allows some variability of the force of infection among the different LGAs acknowledginga t the same time  diferrences  (variability of $\lambda$s and some similarity (values extracted from the same distribution))
Initially we redefine the model and the dataframe for the calibration . In this case we calibrate the model on the seroprevalence by age in each LGA

```{r Bauchi hierachical defining model, warning=FALSE,message=FALSE}
names(Bauchi)[names(Bauchi)=="lga"]<-"LGA"
BauchiLGASero<-Bauchi %>% group_by(age,LGA) %>%  
  dplyr::summarize(Pos = sum(ELISA_status=="positive",na.rm=TRUE),Neg = sum(ELISA_status=="negative",na.rm=TRUE))%>%
  mutate(Perc=100*Pos/(Neg+Pos))%>%
  mutate(
   age_month = case_when(age == "lamb" ~ agelamb,
                    age == "young" ~ ageyoung,
                    age == "mature" ~ agemature,
                    age == "old" ~ ageold)
  )%>% 
  mutate(age=factor(age,levels=c("lamb","young","mature","old"))) %>%
  arrange(age)
BauchiLGASero<-BauchiLGASero[!is.na(BauchiLGASero$LGA),]
BauchiLGASero$offset<-offset_month




hierarcLGACode <- nimbleCode({
  for(i in  1:N) {
    
    # True occupancy status
    pos[i]~dbinom(p[i],tot[i])
  }
  # mixture component parameters drawn from base measures
  
  for(l in 1:Nunique){
    l1[l] ~ dbeta(a1, b1)
    l0[l] ~ dbeta(a0, b0)
  }
  
  for (i in 1:N){
    
    
    age[i]<-agevector[i]

    p[i] <- (1-exp(-l0[LGAs[i]]*(agelamb-age0))*exp(-l1[LGAs[i]]*(age[i]-agelamb)))
    
  }
  # hyperparameters Lambda1
  a1 <- mu1*eta1
  b1 <- (1-mu1)*eta1
  mu1 ~ dbeta(mua1, mub1)
  eta1 <- exp(logeta1)
  logeta1 ~ dlogis(logn1, 1)# hyperparameters Lambda1
  # hyperparameters Lambda0
  a0 <- mu0*eta0
  b0 <- (1-mu0)*eta0
  mu0 ~ dbeta(mua0, mub0)
  eta0 <- exp(logeta0)
  logeta0 ~ dlogis(logn0, 1)# hyperparameters Lambda0
})

listLGA<-unique(BauchiLGASero$LGA)
BauchiLGASero$LGAn<-match(BauchiLGASero$LGA,listLGA)
seroConsts<-list(agevector=BauchiLGASero$age_month,N=length(BauchiLGASero$LGA),age0=BauchiLGASero$offset[1],agelamb=agelamb,LGAs=(BauchiLGASero$LGAn),Nunique=length(unique(BauchiLGASero$LGA)))

seroData<-list(pos=BauchiLGASero$Pos,tot=BauchiLGASero$Pos+BauchiLGASero$Neg)
seroInits<-list(mua1 = 1, mub1 = 1, logn1 = log(100),mua0 = 1, mub0 = 1, logn0 = log(100))

serohModel <- nimbleModel(code = hierarcLGACode, name = 'Serology LGA', constants = seroConsts,
                          data = seroData, inits = seroInits)

#serohMCMC <- buildMCMC(serohModel)
CserohModel <- compileNimble(serohModel)
#CserohMCMC <- compileNimble(serohMCMC, project = serohModel)
conf <- configureMCMC(serohModel, monitors = c('l0','l1'), print = TRUE)
mcmc <- buildMCMC(conf)
cmcmc <- compileNimble(mcmc, project = serohModel)

runMCMC_samples <- runMCMC(cmcmc, nburnin = 2000, niter = 50000,nchains = 20,thin=10)

BauchiLGAcodalist<-post_convert(runMCMC_samples)
```
As in the previous case we chack the diagnostic of the algorithm and the Gelman diag gives a value very close to 1

```{r Bauchi LGA checking diagnostic}
gelman.diag(BauchiLGAcodalist)
#gelman.plot(BauchiLGAcodalist)
```
We summarise the geweke diag to understand if there are chains that are not stationary. To this end we check if for any of the `r length(BauchiLGAcodalist)`  chains, at least for one of the 2 lambdas for each LGA , the geweke value is higher of 1.96. In case it happens we remove the corresponding chain from our analysis

```{r Bauchi LGA geweke test}
gew<-geweke.diag(BauchiLGAcodalist)
count_notconverging<-0
removing<-c()
for(i in 1:length(gew)){
  zlist<-gew[[i]]$z
  if(any(abs(zlist)>1.96)){
    count_notconverging<-count_notconverging+1
    removing<-c(removing,i)
  }
}
runMCMC_samplesred<-runMCMC_samples[-removing]
BauchiLGAcodalist<-post_convert(runMCMC_samplesred)
```

A total of `r count_notconverging` chains over `r length(runMCMC_samples)` have been found not stationary  and removed

Plotting the parameters estimated from  the model

```{r Bauchi LGA hierarchy parameters,warning=FALSE,message=FALSE}
summarypar<-data.frame()
for(l in 1:length(runMCMC_samples)){
  summarypar<-rbind(summarypar,runMCMC_samples[[l]])
}
summaryl0<-summarypar[,1:length(listLGA)]
names(summaryl0)<-listLGA
summaryl0_LGA<-melt(summaryl0,variable.name="LGA",value.name="l0")

summaryl1<-summarypar[,(length(listLGA)+1):(2*length(listLGA))]
names(summaryl1)<-listLGA
summaryl1_LGA<-melt(summaryl1,variable.name="LGA",value.name="l1")
summarypar<-cbind(summaryl0_LGA,summaryl1_LGA)
summarypar<-summarypar[,c(1,2,4)]

summaryparm<-melt(summarypar,id.vars="LGA",value.name = "value",variable.name="Lambda")
BauchiLGAsummaryparmplot<-ggplot(summaryparm, aes(x=LGA, y=value, fill=Lambda)) + 
    geom_boxplot()+coord_flip()
BauchiLGAsummaryparmplot
```

```{r Bauchi LGA hierachy summary value,warning=FALSE}




summarystate<-data.frame()
for(l in 1:length(Bauchicodalist)){
  summarystate<-rbind(summarystate,Bauchicodalist[[l]])
}
summarystate$LGA<-"State-level"
summarystatem<-melt(summarystate,id.vars="LGA",value.name = "value",variable.name="Lambda")

summaryparm<-rbind(summaryparm,summarystatem)

BauchiLGAsummarypartable<-summaryparm %>%
  group_by(LGA,Lambda)%>%
  summarise(mean = mean(value),sd=sd(value),Low95 =quantile(value,probs=.025),Low50 =quantile(value,probs=.25),Up50 =quantile(value,probs=.75),Up95 =quantile(value,probs=.975))

write.csv(BauchiLGAsummarypartable,"../3_Outputs/hierachical_parameters_Bauchi_v2.csv")
```


Plotting the seroprevalence estimated

```{r Bauchi hierarchical model seroprevalence estimated}
nsample<-2000
samplinglist<-data.frame()
for(l in 1:length(runMCMC_samples)){
  samplinglist<-rbind(samplinglist,runMCMC_samples[[l]][sample(nrow(runMCMC_samples[[l]]),size=nsample,replace=TRUE),])
}
samplingl0<-samplinglist[,1:length(listLGA)]
names(samplingl0)<-listLGA
samplingl0_LGA<-melt(samplingl0,variable.name="LGA",value.name="l0")

samplingl1<-samplinglist[,(length(listLGA)+1):(2*length(listLGA))]
names(samplingl1)<-listLGA
samplingl1_LGA<-melt(samplingl1,variable.name="LGA",value.name="l1")
samplingpar<-cbind(samplingl0_LGA,samplingl1_LGA)
samplingpar<-samplingpar[,c(1,2,4)]
SeroLGABauchiPlot<-plotsimLGA(BauchiLGASero,samplingpar)
SeroLGABauchiPlot
```

As in the previous case the red corresponds to the simulated seroprevalence using the catalytic model and with the shaded areas corresponding to 50 and 95 % C.I..  Some improvements can be made